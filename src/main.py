import networkx as nx
import matplotlib.pyplot as plt
import pandas as pd

path = './Data/L_4_dep_c_sparsified_MI_matrix_Transcriptomics.csv'

#f = open(path)
#x = f.readlines() 
#f.close()

df = pd.read_csv(path)
print(df)

G = nx.from_pandas_adjacency(df)
#print(nx.diameter(G))

pos=nx.spring_layout(G)
nx.draw(G, pos=pos, with_labels=True, font_weight='bold')
plt.show()
